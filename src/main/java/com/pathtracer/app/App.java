package com.pathtracer.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class App extends Application {
    public final static double epsilon = 1e-4;
    public static ClassLoader loader = null;

    @Override
    public void start(Stage stage) throws Exception {
        loader = getClass().getClassLoader();
        Parent root = FXMLLoader.load(loader.getResource("menu.fxml"));
        stage.getIcons().add(new Image(loader.getResourceAsStream("icon.png")));
        stage.setTitle("PathTracer");
        stage.setScene(new Scene(root));
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
