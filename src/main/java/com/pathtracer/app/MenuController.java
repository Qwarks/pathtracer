package com.pathtracer.app;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MenuController implements Initializable {
    private final Logger logger = Logger.getLogger("MenuController");

    private final File Default = new File(System.getenv("HOME"));

    private Stage Stage;

    private File Chosen = null;

    @FXML
    private TextField FileText;

    @FXML
    private Button BrowseButton;

    @FXML
    private Button CancelButton;

    @FXML
    private Button RenderButton;

    @FXML
    private AnchorPane MainPane;

    @Override
    @FXML
    public void initialize(URL url, ResourceBundle resource) {
        logger.log(Level.INFO, "Controller Initialization");
        FileText.setText("Please choose a file.");
        BrowseButton.setOnAction(this::BrowseAction);
        RenderButton.setOnAction(this::RenderAction);
        CancelButton.setOnAction(this::CancelAction);
    }

    private void BrowseAction(ActionEvent event) {
        logger.log(Level.INFO, "Browse Event");
        if (Stage == null)
            Stage = (Stage) MainPane.getScene().getWindow();
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(Default);
        File tmp = chooser.showOpenDialog(Stage);
        if (tmp != null) {
            Chosen = tmp;
            FileText.setText(Chosen.getPath());
            RenderButton.setDisable(false);
        }
    }

    private void RenderAction(ActionEvent event) {
        logger.log(Level.INFO, "Render Event");
        if (Stage == null)
            Stage = (Stage) MainPane.getScene().getWindow();
        try {
            FXMLLoader loader = new FXMLLoader(App.loader.getResource("renderer.fxml"));
            Parent root = loader.load();
            javafx.stage.Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setUserData(Chosen);
            RendererController controller = loader.getController();
            controller.setStage(stage);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
        } catch (Exception exception) {
            logger.log(Level.SEVERE, exception.getMessage());
            exception.printStackTrace();
        }
    }

    private void CancelAction(ActionEvent actionEvent) {
        logger.log(Level.INFO, "Cancel Event");
        if (Stage == null)
            Stage = (Stage) MainPane.getScene().getWindow();
        Stage.close();
    }
}
