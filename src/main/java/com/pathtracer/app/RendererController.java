package com.pathtracer.app;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RendererController implements Initializable {
    private final Logger logger = Logger.getLogger("RendererController");

    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    private final File Default = new File(System.getenv("HOME"));

    private final File Output = new File("/tmp/output.png");

    private File Input;

    private Stage Stage;

    @FXML
    private Button SaveButton;

    @FXML
    private Button BackButton;

    @FXML
    private ImageView RenderImageView;

    @FXML
    private AnchorPane MainPane;

    @Override
    @FXML
    public void initialize(URL url, ResourceBundle resource) {
        logger.log(Level.INFO, "Controller Initialization");
        BackButton.setOnAction(this::BackAction);
        SaveButton.setOnAction(this::SaveAction);
    }

    private void shutdown(WindowEvent event) {
        logger.log(Level.INFO, "Shutdown Event");
        executor.shutdown();
    }

    public void setStage(Stage stage) {
        logger.log(Level.INFO, "Setting Stage");
        Stage = stage;
        Stage.setOnHidden(this::shutdown);
        if (Stage.getUserData() instanceof File input)
            Input = input;
        else {
            logger.log(Level.SEVERE, "Wrong instance in UserData.");
            Stage.close();
        }
    }

    private void SaveAction(ActionEvent event) {
        logger.log(Level.INFO, "Save Event");
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(Default);
        chooser.setTitle("Save");
        chooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All Files", "*.*"));
        File chosen = chooser.showSaveDialog(Stage);
        if (chosen != null) {
            Output.renameTo(chosen);
        }
    }

    private void BackAction(ActionEvent event) {
        logger.log(Level.INFO, "Back Event");
        Stage.close();
    }
}
