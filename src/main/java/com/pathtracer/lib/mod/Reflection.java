package com.pathtracer.lib.mod;

public enum Reflection {
    DIFFUSE,
    SPECULAR,
    REFRACTION;
}
