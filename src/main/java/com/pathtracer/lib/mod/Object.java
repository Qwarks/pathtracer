package com.pathtracer.lib.mod;

import com.pathtracer.lib.ray.Ray;
import com.pathtracer.lib.ray.Vector;

public interface Object {
    Vector getPosition();

    Vector getEmission();

    Vector getColor();

    Reflection getReflection();

    double Intersect(Ray ray);
}
