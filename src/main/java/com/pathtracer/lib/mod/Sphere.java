package com.pathtracer.lib.mod;

import com.pathtracer.app.App;
import com.pathtracer.lib.ray.Ray;
import com.pathtracer.lib.ray.Vector;

public class Sphere implements Object {
    private final double radius;
    private final Vector position;
    private final Vector emission;
    private final Vector color;
    private final Reflection reflection;

    public Sphere(double radius, Vector position, Vector emission, Vector color, Reflection reflection) {
        this.radius = radius;
        this.position = position;
        this.emission = emission;
        this.color = color;
        this.reflection = reflection;
    }

    @Override
    public Vector getPosition() {
        return position;
    }

    @Override
    public Vector getEmission() {
        return emission;
    }

    @Override
    public Vector getColor() {
        return color;
    }

    @Override
    public Reflection getReflection() {
        return reflection;
    }

    @Override
    public double Intersect(Ray ray) {
        Vector operator = position.subtract(ray.getOrigin());
        double b = operator.dot(ray.getDirection());
        double determinant = b * b - operator.dot(operator) + radius * radius;
        if (determinant < 0)
            return 0;
        else
            determinant = Math.sqrt(determinant);

        double distance = b - determinant;
        return distance > App.epsilon ? distance : ((distance = b + determinant) > App.epsilon ? distance : 0);
    }
}
