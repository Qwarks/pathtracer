package com.pathtracer.lib.ray;

public class Ray implements Comparable<Ray> {
    private final Vector origin;
    private final Vector direction;

    public Ray(Vector origin, Vector direction) {
        this.origin = origin;
        this.direction = direction;
    }

    public Ray(double originX, double originY, double originZ, double directionX, double directionY, double directionZ) {
        this(new Vector(originX, originY, originZ), new Vector(directionX, directionY, directionZ));
    }

    @Override
    public int compareTo(Ray o) {
        return 0;
    }

    public Vector getOrigin() {
        return origin;
    }

    public Vector getDirection() {
        return direction;
    }
}
