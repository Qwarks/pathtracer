package com.pathtracer.lib.ray;

import javax.validation.constraints.NotNull;
import java.util.Objects;

/**
 * Represents a 3D Vector
 */
public class Vector implements Comparable<Vector> {

    /**
     * Null vector. (0, 0, 0)
     */
    public final static Vector ZERO = new Vector(0, 0, 0);
    /**
     * Unit vector in the X direction. (1, 0, 0)
     */
    public final static Vector UNIT_X = new Vector(1, 0, 0);
    /**
     * Unit vector pointing forward. (1, 0, 0)
     */
    public final static Vector FORWARD = UNIT_X;
    /**
     * Unit vector pointing backward. (-1, 0, 0)
     */
    public final static Vector BACKWARD = scale(UNIT_X, -1);
    /**
     * Unit vector in the Y direction. (0, 1, 0)
     */
    public final static Vector UNIT_Y = new Vector(0, 1, 0);
    /**
     * Unit vector pointing up. (0, 1, 0)
     */
    public final static Vector UP = UNIT_Y;
    /**
     * Unit vector pointing down. (0, -1, 0)
     */
    public final static Vector DOWN = scale(UNIT_Y, -1);
    /**
     * Unit vector in the Z direction. (0, 0, 1)
     */
    public final static Vector UNIT_Z = new Vector(0, 0, 1);
    /**
     * Unit vector pointing right. (0, 0, 1)
     */
    public final static Vector RIGHT = UNIT_Z;
    /**
     * Unit vector pointing left. (0, 0, -1)
     */
    public final static Vector LEFT = scale(UNIT_Z, -1);
    /**
     * Unit vector in all direction. (1, 1, 1)
     */
    public final static Vector ONE = new Vector(1, 1, 1);
    protected final double x, y, z;

    /**
     * Constructs a new vector given x, y, z.
     *
     * @param x x
     * @param y y
     * @param z z
     */
    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Constructs a new vector given x, y, z.
     *
     * @param x x
     * @param y y
     * @param z z
     */
    public Vector(float x, float y, float z) {
        this(x, y, (double) z);
    }

    /**
     * Constructs a new vector given x, y, z.
     *
     * @param x x
     * @param y y
     * @param z z
     */
    public Vector(int x, int y, int z) {
        this(x, y, (double) z);
    }

    /**
     * Constructs a copy of a given vector.
     *
     * @param vector vector
     */
    public Vector(@NotNull Vector vector) {
        this(vector.getX(), vector.getY(), vector.getZ());
    }

    /**
     * Constructs a null vector.
     */
    public Vector() {
        this(0, 0, (double) 0);
    }

    /**
     * Vector's x value.
     *
     * @return The current vector's x value
     */
    public double getX() {
        return x;
    }

    /**
     * Vector's y value.
     *
     * @return The current vector's y value
     */
    public double getY() {
        return y;
    }

    /**
     * Vector's z value.
     *
     * @return The current vector's z value
     */
    public double getZ() {
        return z;
    }

    @Override
    public int compareTo(Vector other) {
        return Vector.compareTo(this, other);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY(), getZ());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vector vector)) return false;
        return Double.compare(vector.getX(), getX()) == 0 && Double.compare(vector.getY(), getY()) == 0 && Double.compare(vector.getZ(), getZ()) == 0;
    }

    @Override
    public String toString() {
        return toString(this);
    }

    /**
     * Vector's length.
     *
     * @return The current vector's length
     */
    public double getLength() {
        return getLength(this);
    }

    /**
     * Vector's length squared.
     *
     * @return The current vector's length squared
     */
    public double getLengthSquared() {
        return getLengthSquared(this);
    }

    /**
     * Adds the other vector to the current vector.
     *
     * @param other The other vector
     * @return The result of the operation
     */
    public Vector add(Vector other) {
        return Vector.add(this, other);
    }

    /**
     * Subtracts the other vector to the current vector.
     *
     * @param other The other vector
     * @return The result of the operation
     */
    public Vector subtract(Vector other) {
        return Vector.subtract(this, other);
    }

    /**
     * Computes the dot product of the current vector and the other vector.
     *
     * @param other The other vector
     * @return The result of the operation
     */
    public double dot(Vector other) {
        return dot(this, other);
    }

    /**
     * Scales by the scalar value.
     *
     * @param scale The scale
     * @return The result of the scaling
     */
    public Vector scale(double scale) {
        return scale(this, scale);
    }

    /**
     * Normalizes the vector.
     *
     * @return The normalized vector
     */
    public Vector normalize() {
        return normalize(this);
    }

    /**
     * Computes the cross product of the current vector and an other vectors.
     *
     * @param other The other vector
     * @return The result of the operation
     */
    public Vector cross(Vector other) {
        return Vector.cross(this, other);
    }

    /**
     * Rounds the current vector's coordinates to their nearest upper integer value.
     *
     * @return The rounded vector
     */
    public Vector ceil() {
        return ceil(this);
    }

    /**
     * Rounds the current vector's coordinates to their nearest lower integer value.
     *
     * @return The rounded vector
     */
    public Vector floor() {
        return floor(this);
    }

    /**
     * Rounds the current vector's coordinates to their nearest integer value.
     *
     * @return The rounded vector
     */
    public Vector round() {
        return round(this);
    }

    /**
     * Computes the absolute value of the current vector's coordinates.
     *
     * @return The vector's absolute value
     */
    public Vector abs() {
        return abs(this);
    }

    /**
     * Returns a vector containing the smallest x, y, and z values in the current vector and the given vector.
     *
     * @param other The other vector
     * @return The resulting vector
     */
    public Vector min(Vector other) {
        return min(this, other);
    }

    /**
     * Returns a vector containing the largest x, y, and z values in the current vector and the given vector.
     *
     * @param other The other vector
     * @return The resulting vector
     */
    public Vector max(Vector other) {
        return max(this, other);
    }

    /**
     * Raises the values of the current vector to the given power.
     *
     * @param power The power
     * @return The resulting vector
     */
    public Vector pow(double power) {
        return pow(this, power);
    }

    /**
     * Compares two vectors.
     *
     * @param first  The first vector
     * @param second The second vector
     * @return The comparison's result (first - second)
     */
    public static int compareTo(Vector first, Vector second) {
        return (int) (getLengthSquared(first) - getLengthSquared(second));
    }

    /**
     * Checks if two vectors are equals.
     *
     * @param first  The first vector
     * @param second The second vector
     * @return True if the vectors are equal, false otherwise
     */
    public static boolean equals(Object first, Object second) {
        if (first instanceof Vector vector)
            return vector.equals(second);
        return false;
    }

    /**
     * Gets the string format of a vector.
     *
     * @param vector The vector
     * @return The string format of the vector
     */
    public static String toString(@NotNull Vector vector) {
        return "{ " + vector.getX() + ", " + vector.getY() + ", " + vector.getZ() + " }";
    }

    /**
     * Computes the length of the given vector.
     *
     * @param vector The vector
     * @return The given vector's length
     */
    public static double getLength(Vector vector) {
        return Math.sqrt(getLengthSquared(vector));
    }

    /**
     * Computes the length squared of the given vector.
     *
     * @param vector The vector
     * @return The given vector's length squared
     */
    public static double getLengthSquared(Vector vector) {
        return dot(vector, vector);
    }

    /**
     * Adds two vectors.
     *
     * @param first  The first vector
     * @param second The second vector
     * @return The result of the operation
     */
    public static Vector add(@NotNull Vector first, @NotNull Vector second) {
        return new Vector(first.getX() + second.getX(), first.getY() + second.getY(), first.getZ() + second.getZ());
    }

    /**
     * Subtracts two vectors. (first - second)
     *
     * @param first  The first vector
     * @param second The second vector
     * @return The result of the operation
     */
    public static Vector subtract(@NotNull Vector first, @NotNull Vector second) {
        return new Vector(first.getX() - second.getX(), first.getY() - second.getY(), first.getZ() - second.getZ());
    }

    /**
     * Computes the dot product of two vectors.
     *
     * @param first  The first vector
     * @param second The second vector
     * @return The result of the operation
     */
    public static double dot(@NotNull Vector first, @NotNull Vector second) {
        return first.getX() * second.getX() + first.getY() * second.getY() + first.getZ() * second.getZ();
    }

    /**
     * Scales the given vector by the given scalar value.
     *
     * @param vector The vector
     * @param scale  The scale
     * @return The result of the scaling
     */
    public static Vector scale(@NotNull Vector vector, double scale) {
        return new Vector(vector.getX() * scale, vector.getY() * scale, vector.getZ() * scale);
    }

    /**
     * Normalizes the given vector.
     *
     * @return The normalized vector
     */
    public static Vector normalize(@NotNull Vector vector) {
        return scale(vector, 1f / getLength(vector));
    }

    //******************************

    /**
     * Computes the cross product of two vectors.
     *
     * @param first  The first vector
     * @param second The second vector
     * @return The result of the operation
     */
    public static Vector cross(Vector first, Vector second) {
        return new Vector(first.getY() * second.getZ() - first.getZ() * second.getY(), first.getZ() * second.getX() - first.getX() * second.getZ(), first.getX() * second.getY() - first.getY() * second.getX());
    }

    /**
     * Rounds the vector's coordinates to their nearest upper integer value.
     *
     * @param vector The vector
     * @return The rounded vector
     */
    public static Vector ceil(Vector vector) {
        return new Vector(Math.ceil(vector.getX()), Math.ceil(vector.getY()), Math.ceil(vector.getZ()));
    }

    /**
     * Rounds the vector's coordinates to their nearest lower integer value.
     *
     * @param vector The vector
     * @return The rounded vector
     */
    public static Vector floor(Vector vector) {
        return new Vector(Math.floor(vector.getX()), Math.floor(vector.getY()), Math.floor(vector.getZ()));
    }

    /**
     * Rounds the vector's coordinates to their nearest integer value.
     *
     * @param vector The vector
     * @return The rounded vector
     */
    public static Vector round(Vector vector) {
        return new Vector(Math.round(vector.getX()), Math.round(vector.getY()), Math.round(vector.getZ()));
    }

    /**
     * Computes the absolute value of the vector's coordinates.
     *
     * @param vector The vector
     * @return The vector's absolute value
     */
    public static Vector abs(Vector vector) {
        return new Vector(Math.abs(vector.getX()), Math.abs(vector.getY()), Math.abs(vector.getZ()));
    }

    /**
     * Returns a vector containing the smallest x, y, and z values in the two vectors.
     *
     * @param first  The first vector
     * @param second The second vector
     * @return The resulting vector
     */
    public static Vector min(Vector first, Vector second) {
        return new Vector(Math.min(first.getX(), second.getX()), Math.min(first.getY(), second.getY()), Math.min(first.getZ(), second.getZ()));
    }

    /**
     * Returns a vector containing the largest x, y, and z values in the two vectors.
     *
     * @param first  The first vector
     * @param second The second vector
     * @return The resulting vector
     */
    public static Vector max(Vector first, Vector second) {
        return new Vector(Math.max(first.getX(), second.getX()), Math.max(first.getY(), second.getY()), Math.max(first.getZ(), second.getZ()));
    }

    /**
     * Returns a random vector. (values between 0 and 1)
     *
     * @return The random vector
     */
    public static Vector rand() {
        return new Vector(Math.random(), Math.random(), Math.random());
    }

    /**
     * Raises the values of a vector to the given power.
     *
     * @param vector The vector
     * @param power  The power
     * @return The resulting vector
     */
    public static Vector pow(Vector vector, double power) {
        return new Vector(Math.pow(vector.getX(), power), Math.pow(vector.getY(), power), Math.pow(vector.getZ(), power));
    }
}
